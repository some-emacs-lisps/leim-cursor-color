;;; leim-cursor-color.el --  Specify current input mode: In leim or out of leim.

;; Author: Takayuki YAMAGUCHI <d@ytak.info>
;; Keywords: leim cursor color
;; Version: 0.0.1
;; Created: Sat Oct 19 12:09:05 2013
;; Site: https://gitorious.org/some-emacs-lisps/leim-cursor-color

;; leim-cursor-color.el change color of cursor according to the variable `current-input-method'.
;; When we change focus on window and so on, leim-cursor-color.el sets timer to change color of cursor.
;; This implementation is to avoid too many callings of functions.

;; Copyright 2013 Takayuki YAMAGUCHI

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3 of the License, or (at your option) any later 
;; version. 
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT 
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 
;; 
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <http://www.gnu.org/licenses/>.

;;; Usage:
;; (require 'leim-cursor-color)
;; (setq leim-cursor-color-none "lemon chiffon")
;; (setq leim-cursor-color-alist
;;       '(("japanese-mozc" . "blue") ("japanese-katakana" . "green")))
;; (leim-cursor-color-init)

;; ChangeLog:
;; 2013/10/19 version 0.0.1 yamaguchi
;;     Initial version

;;; Code:

(defvar leim-cursor-color-no-input-method (cdr (assoc 'cursor-color (frame-parameters (selected-frame))))
  "Cursor color when input method is off")

(defvar leim-cursor-color-alist '(("japanese-mozc" . "blue"))
  "Cursor colors for input methods")

(defvar leim-cursor-color-set-timer nil)

(defvar leim-cursor-color-wait-time 1)

(defvar leim-cursor-color-hooks-to-set-color
  '(input-method-activate-hook input-method-inactivate-hook))

(defvar leim-cursor-color-functions-to-set-timer
  '(bury-buffer kill-buffer switch-to-buffer select-window other-window))

(defun leim-cursor-color-set ()
  (let ((color (or (cdr (assoc current-input-method leim-cursor-color-alist))
                   leim-cursor-color-no-input-method)))
    (set-cursor-color color)))

(defun leim-cursor-color-set-timer-create ()
  (unless leim-cursor-color-set-timer
    (setq leim-cursor-color-set-timer
          (run-with-idle-timer
           leim-cursor-color-wait-time nil
           (lambda ()
             (leim-cursor-color-set)
             (setq leim-cursor-color-set-timer nil))))))

(defun leim-cursor-color-set-lazy-in-minibuffer ()
  (if (window-minibuffer-p)
      (leim-cursor-color-set-timer-create)
    (leim-cursor-color-set)))

(defun leim-cursor-color-init ()
  (dolist (hook leim-cursor-color-hooks-to-set-color)
    (add-hook hook 'leim-cursor-color-set-lazy-in-minibuffer))
  (dolist (func leim-cursor-color-functions-to-set-timer)
    (eval `(defadvice ,func (after add-leim-cursor-set-timer first activate)
             (leim-cursor-color-set-timer-create)))))

(provide 'leim-cursor-color)

;;; leim-cursor-color.el ends here
